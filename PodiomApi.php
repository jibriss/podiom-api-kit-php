<?php
require_once 'PodiomApiException.php';

class PodiomApi
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $host;

    private $lastRequest;

    public function __construct($key = null, $host = 'api.podiom.fr')
    {
        if (!function_exists('curl_init')) {
            throw new PodiomApiException("Podiom API Kit requires CURL extension");
        }

        if (!function_exists('json_decode')) {
            throw new PodiomApiException("Podiom API Kit requires JSON extension");
        }

        if (!function_exists('hash') || !in_array('sha256', hash_algos())) {
            throw new PodiomApiException("The 'sha256' hash algorithm is not available on your PHP installation");
        }

        $this->key  = $key;
        $this->host = $host;
    }

    public function getAllUsers()
    {
        return $this->send('GET', "/v1/users");
    }

    public function getUser($id)
    {
        return $this->send('GET', "/v1/user/$id");
    }

    public function getUserBadges($id)
    {
        return $this->send('GET', "/v1/user/$id/badges");
    }

    public function postUser($user)
    {
        return $this->send('POST', "/v1/user", $user);
    }

    public function getAllBadges()
    {
        return $this->send('GET', "/v1/badges");
    }

    public function getBadge($id)
    {
        return $this->send('GET', "/v1/badge/$id");
    }

    public function getBadgeUsers($id)
    {
        return $this->send('GET', "/v1/badge/$id/users");
    }

    public function postBadge($badge)
    {
        return $this->send('POST', "/v1/badge", $badge);
    }

    public function grantBadge($userId, $badgeId)
    {
        $grant = array('user_id' => $userId, 'badge_id' => $badgeId);
        return $this->send('POST', "/v1/grant", $grant);
    }

    public function getIssuer($id)
    {
        return $this->send('GET', "/v1/issuer/$id");
    }

    public function postIssuer($issuer)
    {
        return $this->send('POST', "/v1/issuer", $issuer);
    }


    /**
     * Internals
     */

    public function getLastRequest()
    {
        return $this->lastRequest;
    }

    protected function send($method, $resource, array $params = array())
    {
        array_walk($params, array($this, 'normalizeParam'));
        $url = 'http://' . $this->host . $resource;

        $this->lastRequest = array(
            'request_method' => $method,
            'request_url' => $url,
            'request_params' => $params
        );

        list($json, $code) = $this->doCurl($method, $url, $params);

        $this->lastRequest['response_code'] = $code;
        $this->lastRequest['response_body'] = $json;

        return json_decode($json, true);
    }

    private function normalizeParam(&$param)
    {
        if (is_bool($param)) {
            $param = (int)$param;
        }

        return $param;
    }

    private function computeSignature($resource, $params)
    {
        $params['url'] = $resource;
        $params['api_secret'] = $this->secret;
        ksort($params);
        return hash('sha256', implode('', $params));
    }

    private function doCurl($method, $url, array $params)
    {
        $headers = array(
            'Accept: application/json'
        );

        if (isset($this->key)) {
            $headers[] = 'X-Podiom-Api-Key: ' . $this->key;
        }

        $this->lastRequest['request_headers'] = $headers;

        $curly = curl_init();

        if ($method == 'GET' || $method == 'DELETE') {
            $url .= '?' . http_build_query($params);
        } else {
            curl_setopt($curly, CURLOPT_POSTFIELDS, http_build_query($params));
        }

        curl_setopt($curly, CURLOPT_HEADER, 0);
        curl_setopt($curly, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curly, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curly, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curly, CURLOPT_URL, $url);

        $responseBody = curl_exec($curly);
        $responseCode = curl_getinfo($curly, CURLINFO_HTTP_CODE);

        if ($responseBody === false) {
            $e = new PodiomApiException(sprintf("Curl error #%s : %s", curl_error($curly), curl_errno($curly)));
            curl_close($curly);
            throw $e;
        }

        curl_close($curly);

        return array($responseBody, $responseCode);
    }
}
